package com.smarthome.smartdevice;

import devices.MqttDevice;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import sensors.eCO2Sensor;
import sensors.HumiditySensor;
import sensors.TemperatureSensor;

import java.util.List;
import java.util.Optional;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void stop(Promise<Void> stopPromise) throws Exception {
    System.out.println("Device stopped");
    stopPromise.complete();
  }

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    /*
      Define parameters of the application
      ------------------------------------
     */

    var deviceType = Optional.ofNullable(System.getenv("DEVICE_TYPE")).orElse("mqtt");
    var deviceLocation = Optional.ofNullable(System.getenv("DEVICE_LOCATION")).orElse("somewhere");
    var deviceId = Optional.ofNullable(System.getenv("DEVICE_ID")).orElse("something");

    var mqttTopic = Optional.ofNullable(System.getenv("MQTT_TOPIC")).orElse("house");

    /*
      Initialize the device (new HttpDevice(deviceId))
     */
    var mqttDevice = new MqttDevice(deviceId)
      .setCategory(deviceType)
      .setPosition(deviceLocation)
      .setSensors(List.of(
        new TemperatureSensor(),
        new HumiditySensor(),
        new eCO2Sensor()
      ));

    mqttDevice.startAndConnectMqttClient(vertx)
      .onFailure(error -> {
        System.out.println("🤖 " + error.getMessage());
      })
      .onSuccess(ok -> {

        vertx.setPeriodic(5000, handler -> {
          mqttDevice.getMqttClient().publish(mqttTopic,
            Buffer.buffer(mqttDevice.jsonValue().encode()),
            MqttQoS.AT_LEAST_ONCE, // AT_LEAST_ONCE
            false,
            false
          );
        });

      });



  }

}
