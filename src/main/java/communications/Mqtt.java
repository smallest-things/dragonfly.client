package communications;

import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;
import io.vertx.mqtt.messages.MqttConnAckMessage;

import java.util.Optional;

public interface Mqtt {
  // This is a work in progress
  // This part will be handled during milestone 2
  default String getProtocol() {
    return "mqtt";
  }

  default MqttClient createMqttClient(Vertx vertx) {
    var mqttClientId = Optional.ofNullable(System.getenv("MQTT_CLIENT_ID")).orElse("mqttDevice");

    return MqttClient.create(vertx, new MqttClientOptions()
      .setClientId(mqttClientId)
    ).exceptionHandler(throwable -> {
      // Netty ?
      System.out.println(throwable.getMessage());

    }).closeHandler(voidValue -> {
      // Connection with broker lost
      // Try to reconnect
      System.out.println("Connection with broker lost");
      startAndConnectMqttClient(vertx);
    });
  };

  MqttClient getMqttClient();
  void setMqttClient(MqttClient client);

  default Future<MqttConnAckMessage> startAndConnectMqttClient(Vertx vertx) {
    var mqttPort = Integer.parseInt(Optional.ofNullable(System.getenv("MQTT_PORT")).orElse("1883"));
    var mqttHost = Optional.ofNullable(System.getenv("MQTT_HOST")).orElse("localhost");

    return getBreaker(vertx).execute(promise -> {

      var mqttClient = createMqttClient(vertx);

      mqttClient.connect(mqttPort, mqttHost)
        .onFailure(error -> {
          System.out.println("MQTT " + error.getMessage());
          promise.fail("[" + error.getMessage() + "]");
        })
        .onSuccess(ok -> {
          System.out.println("The connection looks good");
          setMqttClient(mqttClient);
          promise.complete();
        })
        .onComplete(promise);

    });
  };

  default CircuitBreaker getBreaker(Vertx vertx) {

    return CircuitBreaker.create("device-circuit-breaker", vertx,
      new CircuitBreakerOptions()
        .setMaxFailures(3) // number of failure before opening the circuit
        .setMaxRetries(20)
        .setTimeout(5_000) // consider a failure if the operation does not succeed in time
        .setResetTimeout(10_000) // time spent in open state before attempting to re-try
    ).retryPolicy(retryCount -> retryCount * 100L);
  }

}
